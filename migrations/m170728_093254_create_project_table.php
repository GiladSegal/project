<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170728_093254_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'Name' => $this->string(),
            'customerId' => $this->integer(),
            'owner' => $this->integer(),
            'startTime' => $this->integer(),
            'endTime' => $this->integer(),
            'status' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
