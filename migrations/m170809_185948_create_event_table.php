<?php

use yii\db\Migration;

/**
 * Handles the creation of table `event`.
 */
class m170809_185948_create_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'created_date' => $this->date(),
            'mission' => $this->integer(),
            'project' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('event');
    }
}
