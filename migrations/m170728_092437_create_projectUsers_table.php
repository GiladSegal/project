<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projectUsers`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `project`
 */
class m170728_092437_create_projectUsers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projectUsers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'cproject_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-projectUsers-user_id',
            'projectUsers',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-projectUsers-user_id',
            'projectUsers',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `cproject_id`
        $this->createIndex(
            'idx-projectUsers-cproject_id',
            'projectUsers',
            'cproject_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-projectUsers-cproject_id',
            'projectUsers',
            'cproject_id',
            'project',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-projectUsers-user_id',
            'projectUsers'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-projectUsers-user_id',
            'projectUsers'
        );

        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-projectUsers-cproject_id',
            'projectUsers'
        );

        // drops index for column `cproject_id`
        $this->dropIndex(
            'idx-projectUsers-cproject_id',
            'projectUsers'
        );

        $this->dropTable('projectUsers');
    }
}
