<?php

use yii\db\Migration;

/**
 * Handles the creation of table `missionUsers`.
 * Has foreign keys to the tables:
 *
 * - `mission`
 * - `user`
 */
class m170728_102919_create_missionUsers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('missionUsers', [
            'id' => $this->primaryKey(),
            'mission_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `mission_id`
        $this->createIndex(
            'idx-missionUsers-mission_id',
            'missionUsers',
            'mission_id'
        );

        // add foreign key for table `mission`
        $this->addForeignKey(
            'fk-missionUsers-mission_id',
            'missionUsers',
            'mission_id',
            'mission',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-missionUsers-user_id',
            'missionUsers',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-missionUsers-user_id',
            'missionUsers',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `mission`
        $this->dropForeignKey(
            'fk-missionUsers-mission_id',
            'missionUsers'
        );

        // drops index for column `mission_id`
        $this->dropIndex(
            'idx-missionUsers-mission_id',
            'missionUsers'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-missionUsers-user_id',
            'missionUsers'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-missionUsers-user_id',
            'missionUsers'
        );

        $this->dropTable('missionUsers');
    }
}
