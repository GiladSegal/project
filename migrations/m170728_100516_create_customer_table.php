<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m170728_100516_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'company' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'address' => $this->string(),
            'phoneNumber' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customer');
    }
}
