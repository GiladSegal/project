<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projectMissions`.
 * Has foreign keys to the tables:
 *
 * - `project`
 * - `mission`
 */
class m170728_102635_create_projectMissions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projectMissions', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'mission_id' => $this->integer()->unique(),
        ]);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-projectMissions-project_id',
            'projectMissions',
            'project_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-projectMissions-project_id',
            'projectMissions',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        // creates index for column `mission_id`
        $this->createIndex(
            'idx-projectMissions-mission_id',
            'projectMissions',
            'mission_id'
        );

        // add foreign key for table `mission`
        $this->addForeignKey(
            'fk-projectMissions-mission_id',
            'projectMissions',
            'mission_id',
            'mission',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-projectMissions-project_id',
            'projectMissions'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            'idx-projectMissions-project_id',
            'projectMissions'
        );

        // drops foreign key for table `mission`
        $this->dropForeignKey(
            'fk-projectMissions-mission_id',
            'projectMissions'
        );

        // drops index for column `mission_id`
        $this->dropIndex(
            'idx-projectMissions-mission_id',
            'projectMissions'
        );

        $this->dropTable('projectMissions');
    }
}
