<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mission`.
 */
class m170728_093455_create_mission_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mission', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'owner' => $this->integer(),
            'startTime' => $this->integer(),
            'realEndTime' => $this->integer(),
            'requestedEndTime' => $this->integer(),
            'status' => $this->string(),
            'body' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mission');
    }
}
