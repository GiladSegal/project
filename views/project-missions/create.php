<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectMissions */

$this->title = 'Connect Missions to an exsisting Project';
$this->params['breadcrumbs'][] = ['label' => 'Project Missions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-missions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
