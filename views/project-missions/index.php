<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectMissionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Missions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-missions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project Missions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'project_id',
            'mission_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
