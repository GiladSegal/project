<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Customer;
use app\models\User;
use app\models\Status;



/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'customerId')->textInput() ?>
	<?= $form->field($model, 'customerId')->
				dropDownList(Customer::getCustomers()) ?>    


    <?//= $form->field($model, 'owner')->textInput() ?>
	<?= $form->field($model, 'owner')->
				dropDownList(User::getUsers()) ?>    


    <?= $form->field($model, 'startTime')->textInput() ?>

    <?= $form->field($model, 'endTime')->textInput() ?>

    <?//= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>  


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	
</div>		


<div class="breadcrumb" align="center">
			<h2> More Actions </h2>

			<div class="col-lg-4">
               <h2>Create new missions for this Project</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a onclick="window.open('http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=mission%2Fcreate', 'newwindow', 'width=800,height=800'); return false;" 
				class="btn btn-default" >Lets Go!</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Connect existing missions to this Project</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a onclick="window.open('http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=project-missions%2Fcreate', 'newwindow', 'width=500,height=500'); return false;" 
				class="btn btn-default" >Lets Go!</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Add users for this project's missions</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a onclick="window.open('http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=mission-users%2Fcreate', 'newwindow', 'width=500,height=500'); return false;" 
					class="btn btn-default" >Lets Go!</a></p>
            </div>
</div>

	
<div id="checkboxConnect">	
<input type="checkbox" value="0"> connect missions from this page
</div>

<script>
///////////////////////not working yet
	document.getElementById('checkboxConnect').onclick = function() {
    // access properties using this keyword
    if ( this.checked ) {
		console.log("checked!");
	   // if checked ...
        alert( this.value );
    } else {
        // if not checked ...
    }
};

</script>
	
<?php if(connectMissions.value == "1"){	?>
	<iframe scrolling="no" src="http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=project-missions%2Fcreate" style="border: 0px none; margin-left: 0px; height: 380px; margin-top: auto; width: 100% ;"></iframe>

	<?php } ?>


