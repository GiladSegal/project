<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MissionUsers */

$this->title = 'Connect Users to an existing Mission';
$this->params['breadcrumbs'][] = ['label' => 'Mission Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mission-users-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
