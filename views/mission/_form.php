<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\Mission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mission-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'owner')->textInput() ?>
	
	<?= $form->field($model, 'owner')->
				dropDownList(User::getUsers()) ?>    

    <?= $form->field($model, 'startTime')->textInput()->input('startTime', ['placeholder' => "__-__-__"]) ?>

    <?= $form->field($model, 'realEndTime')->textInput() ?>

    <?= $form->field($model, 'requestedEndTime')->textInput() ?>

    <?//= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	
	<div class="breadcrumb" align="center">
			<h2> More Actions </h2>

			<div class="col-lg-4">
               <h2>Add users to existing missions</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a onclick="window.open('http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=mission-users%2Fcreate', 'newwindow', 'width=500,height=500'); return false;" 
				class="btn btn-default" >Lets Go!</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Connect missions to a Project</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a onclick="window.open('http://giladse.myweb.jce.ac.il/project/basic/web/index.php?r=project-missions%2Fcreate', 'newwindow', 'width=500,height=500'); return false;" 
				class="btn btn-default" >Lets Go!</a></p>
            </div>

</div>

</div>
