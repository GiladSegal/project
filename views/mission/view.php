<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mission */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Missions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mission-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'owner',
            'startTime:datetime',
            'realEndTime:datetime',
            'requestedEndTime:datetime',
            'status',
            'body:ntext',
			[ // mission created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
            'updated_at',
            [ // mission created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],
            [ // mission updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			[ // mission updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
        ],
    ]) ?>

</div>
