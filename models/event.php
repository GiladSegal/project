<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *new model for calander
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $created_date
 * @property integer $mission
 * @property integer $project
 */
class event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_date'], 'safe'],
            [['mission', 'project'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'created_date' => 'Created Date',
            'mission' => 'Mission',
            'project' => 'Project',
        ];
    }
}
