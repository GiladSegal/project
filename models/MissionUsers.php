<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "missionUsers".
 *
 * @property integer $id
 * @property integer $mission_id
 * @property integer $user_id
 */
class MissionUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'missionUsers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mission_id'], 'required'],
            [['mission_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mission_id' => 'Mission ID',
            'user_id' => 'User ID',
        ];
    }
}
