<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projectMissions".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $mission_id
 */
class ProjectMissions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projectMissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'required'],
            [['project_id', 'mission_id'], 'integer'],
            [['mission_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'mission_id' => 'Mission ID',
        ];
    }
}
