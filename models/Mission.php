<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mission".
 *
 * @property integer $id
 * @property string $title
 * @property integer $owner
 * @property integer $startTime
 * @property integer $realEndTime
 * @property integer $requestedEndTime
 * @property string $status
 * @property string $body
 */
class Mission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'startTime', 'realEndTime', 'requestedEndTime','created_at','updated_at','created_by','updated_by'], 'integer'],
            [['body'], 'string'],
            [['title', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'owner' => 'Owner',
            'startTime' => 'Start Time',
            'realEndTime' => 'Real End Time',
            'requestedEndTime' => 'Requested End Time',
            'status' => 'Status',
            'body' => 'Body',
			'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
		public function getMissionOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

}
