<?php

namespace app\models;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $Name
 * @property integer $customerId
 * @property integer $owner
 * @property integer $startTime
 * @property integer $endTime
 * @property string $status
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
   public function rules()
    {
        return [
            [['customerId', 'owner', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['startTime', 'endTime'], 'date' ],
            
            [['Name', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'customerId' => 'Customer ID',
            'owner' => 'Owner',
            'startTime' => 'Start Time',
            'endTime' => 'End Time',
            'status' => 'Status',
        ];
    }
    
    public function getProjectOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
    
    public function getProjectCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customerId']);
    }
    
    /*  public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
        
        if ($this->isNewRecord)
            $this->status = 1;
        
        return $return;
    }
    */
}
